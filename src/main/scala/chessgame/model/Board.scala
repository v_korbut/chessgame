package chessgame.model

import chessgame.BoardPrinter.{columnNames, rowIndexToName}
import chessgame.model.Board.{Cell, FILES_IN_ROW, ROWS_ON_BOARD}

case class Position(column: Int, row: Int) {
  //TODO: create auxiliary method which parse position
  assert(row < ROWS_ON_BOARD && row >= 0)
  assert(column < FILES_IN_ROW && column >= 0)

  override def toString: String = s"${columnNames(column)}${rowIndexToName(row)}"
}

case class Move(source: Position, target: Position) {
  lazy val colDelta: Int = target.column - source.column
  lazy val rowDelta: Int = target.row - source.row

  def absRowDelta: Int = Math.abs(rowDelta)

  def absColDelta: Int = Math.abs(colDelta)

  lazy val isDiagonal: Boolean = absColDelta == absRowDelta

  def isEnemyDirection(player: PieceColor): Boolean =
    rowDelta > 0 && player == BlackPiece ||
      rowDelta < 0 && player == WhitePiece

  lazy val steps: Seq[Position] = {
    val cd = colDelta
    val rd = rowDelta

    val stepsCount = Math.max(absColDelta, absRowDelta)

    if ((cd == 0 || rd == 0 || isDiagonal) && stepsCount > 0) {

      val columnIncrement = Integer.signum(cd)
      val rowIncrement = Integer.signum(rd)

      val affectedCellsCount = stepsCount + 1
      val columns = (if (columnIncrement == 0) List.empty[Int]
                     else Range.inclusive(source.column, target.column, columnIncrement).toList)
        .padTo(affectedCellsCount, source.column)
      val rows = (if (rowIncrement == 0) List.empty[Int]
                  else Range.inclusive(source.row, target.row, rowIncrement).toList)
        .padTo(affectedCellsCount, source.row)

      columns.zip(rows).tail.map(Function.tupled(Position)).ensuring(_.last == target)
    } else {
      Seq.empty
    }
  }

  override def toString: String = s"$source$target"
}

case class PieceOnBoard(position: Position, piece: Piece)

class Board private (cells: Seq[Cell]) {

  def indexToPosition(position: Int): Position = {
    val col = position % FILES_IN_ROW
    val row = position / FILES_IN_ROW
    Position(col, row)
  }

  def allPieces: Seq[PieceOnBoard] = cells.zipWithIndex.collect {
    case (Some(cell), position: Int) => PieceOnBoard(indexToPosition(position), cell)
  }

  def rows(): Seq[Seq[Cell]] = cells.sliding(FILES_IN_ROW, FILES_IN_ROW).toSeq

  private def positionToIndex(position: Position) = position.row * FILES_IN_ROW + position.column

  def apply(position: Position): Cell = cells(positionToIndex(position))

  def move(start: Position, end: Position): Board = {
    new Board(
      cells
        .updated(positionToIndex(end), cells(positionToIndex(start)))
        .updated(positionToIndex(start), None))

  }

  def put(position: Position, piece: Piece): Board =
    new Board(cells.updated(positionToIndex(position), Some(piece)))
}

object Board {

  val FILES_IN_ROW = 8
  val ROWS_ON_BOARD = 8

  val LAST_ROW: Int = ROWS_ON_BOARD - 1

  type Cell = Option[Piece]

  def initialBoard: Board = {
    val backRow: Seq[PieceType] = Seq(Rook, Knight, Bishop, Queen, King, Bishop, Knight, Rook)
    assert(backRow.length == FILES_IN_ROW)
    val frontRow: Seq[PieceType] = Seq.fill(FILES_IN_ROW)(Pawn)

    def emptyRow: Seq[Cell] = frontRow.map(_ => None)
    def cellsOfPieces(pieceColor: PieceColor, row: Seq[PieceType]): Seq[Cell] =
      row.map(t => Some(Piece(pieceColor, t)))

    val cells = Seq(
      cellsOfPieces(BlackPiece, backRow),
      cellsOfPieces(BlackPiece, frontRow),
      emptyRow,
      emptyRow,
      emptyRow,
      emptyRow,
      cellsOfPieces(WhitePiece, frontRow),
      cellsOfPieces(WhitePiece, backRow)
    ).ensuring(_.length == ROWS_ON_BOARD)

    new Board(cells.flatten)
  }

  def empty = new Board(Seq.fill(FILES_IN_ROW * ROWS_ON_BOARD)(None))
}
