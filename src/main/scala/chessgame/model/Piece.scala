package chessgame.model

sealed trait PieceColor {
  def opponent: PieceColor
}

case object BlackPiece extends PieceColor {
  override def opponent: PieceColor = WhitePiece
}

case object WhitePiece extends PieceColor {
  override def opponent: PieceColor = BlackPiece
}

//@formatter:off
sealed trait PieceType extends Product {
  def initialRowForBlack: Int =
    this match {
      case Pawn => 1
      case _ => 0
    }
}

case object King extends PieceType
case object Queen extends PieceType
case object Rook extends PieceType
case object Knight extends PieceType
case object Bishop extends PieceType
case object Pawn extends PieceType
//@formatter:on

case class Piece(color: PieceColor, pieceType: PieceType) {
  def initialRow: Int =
    if (color == BlackPiece) pieceType.initialRowForBlack
    else Board.LAST_ROW - pieceType.initialRowForBlack
}
