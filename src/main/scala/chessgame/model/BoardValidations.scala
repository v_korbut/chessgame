package chessgame.model

import chessgame.model.Board.Cell

import scala.language.implicitConversions

class BoardValidations(val board: Board) extends AnyVal {

  //TODO: zero move impossible by next rules. consider remove
  def validateZeroMove(move: Move): Either[MoveValidationError, Unit] = {
    if (move.source == move.target) Left(EmptyMove)
    else Right(())
  }

  def validateSourcePosition(move: Move, player: PieceColor): Either[MoveValidationError, Piece] = {
    board(move.source)
      .filter(_.color == player)
      .toRight(NothingToMove(move.source))
  }

  def validateDestinationPosition(move: Move, player: PieceColor): Either[TargetIsOccupied, Cell] = {
    val targetCell = board(move.target)
    targetCell
      .filter(_.color == player)
      .map(_ => TargetIsOccupied(move.target))
      .toLeft(targetCell)
  }

  def validateDirection(piece: Piece, move: Move): Either[MoveValidationError, Piece] = {
    val destination: Cell = board(move.target)

    if (destination.exists(_.color == piece.color))
      Left(IllegalMove(piece))
    else
      piece.pieceType match {
        case King => validateKingMove(piece, move)
        case Queen => validateQueenMove(piece, move)
        case Rook => validateRookMove(piece, move)
        case Bishop => validateBishopMove(piece, move)
        case Knight => validateKnightMove(piece, move)
        case Pawn => validatePawnMove(piece, move)
      }
  }

  def validateKingMove(piece: Piece, move: Move): Either[MoveValidationError, Piece] = {
    assert(piece.pieceType == King) //TODO: improve types in order to prevent this assert.
    if (move.absColDelta >= 2 || move.absRowDelta >= 2) Left(IllegalMove(piece))
    else Right(piece)
  }

  def validateQueenMove(piece: Piece, move: Move): Either[MoveValidationError, Piece] = {
    assert(piece.pieceType == Queen) //TODO: improve types in order to prevent this assert.
    validateQueenLikeMove(piece, move)
  }

  def validateQueenLikeMove(piece: Piece, move: Move): Either[MoveValidationError, Piece] = {
    if (move.steps.isEmpty) Left(IllegalMove(piece))
    else {
      if (move.steps.init.map(board.apply).forall(_.isEmpty)) Right(piece)
      else Left(IllegalMove(piece))
    }
  }

  def validateRookMove(piece: Piece, move: Move): Either[MoveValidationError, Piece] = {
    assert(piece.pieceType == Rook) //TODO: improve types in order to prevent this assert.
    if (move.isDiagonal) Left(IllegalMove(piece))
    else validateQueenLikeMove(piece, move)
  }

  def validateBishopMove(piece: Piece, move: Move): Either[MoveValidationError, Piece] = {
    assert(piece.pieceType == Bishop) //TODO: improve types in order to prevent this assert.
    if (!move.isDiagonal) Left(IllegalMove(piece))
    else validateQueenLikeMove(piece, move)
  }

  def validateKnightMove(piece: Piece, move: Move): Either[MoveValidationError, Piece] = {
    assert(piece.pieceType == Knight) //TODO: improve types in order to prevent this assert.
    if (move.absRowDelta == 2 && move.absColDelta == 1 || move.absRowDelta == 1 && move.absColDelta == 2) Right(piece)
    else Left(IllegalMove(piece))
  }

  def validatePawnMove(piece: Piece, move: Move): Either[MoveValidationError, Piece] = {
    assert(piece.pieceType == Pawn) //TODO: improve types in order to prevent this assert.
    val steps = move.steps

    val illegalMove = Left(IllegalMove(piece))
    val legalMove = Right(piece)

    if (!move.isEnemyDirection(piece.color) || move.absRowDelta > 2 || move.absRowDelta == 0) illegalMove
    else {

      val doubleStepIsNotFirst = move.absRowDelta == 2 && piece.initialRow != move.source.row
      val isStraitMove = move.colDelta == 0
      val pathNotFree = steps.map(board.apply).exists(_.isDefined)

      if (isStraitMove && (doubleStepIsNotFirst || pathNotFree) ||
          (move.isDiagonal && steps.size != 1)) illegalMove
      else legalMove
    }
  }

  def validateMove(move: Move, currentTurn: PieceColor): Either[MoveValidationError, (Piece, Cell)] = {
    for {
      _           <- validateZeroMove(move)
      pieceToMove <- validateSourcePosition(move, currentTurn)
      target      <- validateDestinationPosition(move, currentTurn)
      _           <- validateDirection(pieceToMove, move)
    } yield pieceToMove -> target
  }

  def testInCheck(kingPosition: Position, opponentPositions: Seq[Position], opponentColor: PieceColor): Option[Move] = {
    opponentPositions
      .map(Move(_, kingPosition))
      .view
      .find(move => validateMove(move, opponentColor).isRight)
  }

  def findPlayerInCheck: Map[PieceColor, Move] = {
    val (white, black) = board.allPieces.partition { _.piece.color == WhitePiece }

    val whiteKing = white.find(_.piece.pieceType == King).map(_.position)
    val blackKing = black.find(_.piece.pieceType == King).map(_.position)

    val whiteInCheck = whiteKing
      .flatMap(testInCheck(_, black.map(_.position), BlackPiece))
      .map(WhitePiece -> _)

    val blackInCheck = blackKing
      .flatMap(
        testInCheck(_, white.map(_.position), WhitePiece)
          .map(BlackPiece -> _))

    (whiteInCheck.toList ++ blackInCheck.toList).toMap
  }

}

object BoardValidations {
  implicit def boardValidations(board: Board): BoardValidations = new BoardValidations(board)
}

trait MoveValidationError {
  def errorMessage: String
}

case class Check(causedBy: Move) extends MoveValidationError {
  val errorMessage: String = s"Check. This move is not allowed as the King still under attack $causedBy"
}

case object EmptyMove extends MoveValidationError {
  val errorMessage: String = s"Cannot move at the same position as source one"
}

case class IllegalMove(pieceName: String) extends MoveValidationError {
  val errorMessage: String = s"This move is not allowed for $pieceName"
}

object IllegalMove {
  def apply(piece: Piece) = new IllegalMove(piece.toString)
}

case class NothingToMove(start: Position) extends MoveValidationError {
  val errorMessage: String = s"No piece at start position: $start"
}

case class TargetIsOccupied(target: Position) extends MoveValidationError {
  val errorMessage: String = s"Target position ($target) contains own piece. "
}
