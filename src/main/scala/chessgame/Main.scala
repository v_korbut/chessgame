package chessgame

import chessgame.model._

import scala.annotation.tailrec

object Main extends App {

  val inputFile = args.headOption.map(new java.io.File(_))

  inputFile match {
    case Some(file) if file.isFile && file.canRead => validateGameFromFile(file)
    case Some(file) => println(s"cannot open file for read ${file.getAbsolutePath}")
    case None => println("please specify input file")
  }

  def validateGameFromFile(sourceFile: java.io.File): Unit = {
    println(s"reading from $sourceFile")
    val input = InputFileReader.readFile(sourceFile)

    val result = validateNextTurn(input, new Game())

    printGame(result)
  }

  @tailrec
  def validateNextTurn(input: LazyList[Move], game: Game): Game = {
    input.headOption.map { move =>
      printGame(game)
      println(s"\n> $move")

      game.applyMove(move) match {
        case Left(error) => println("error: " + error.errorMessage); game
        case Right(newGame) => newGame
      }
    } match {
      case Some(newGame) => validateNextTurn(input.tail, newGame)
      case None => game
    }
  }

  def printGame(game: Game): Unit = {
    println(s"-------------")
    println(s"Current turn: ${pieceColorToName(game.currentPlayer)}")
    println(if (game.inCheck) "In check" else "")
    println()

    BoardPrinter.printBoard(game.board)
  }

  def pieceColorToName(currentPlayer: PieceColor) =
    currentPlayer match {
      case WhitePiece => "white"
      case BlackPiece => "black"
    }
}
