package chessgame

import chessgame.model._

object BoardPrinter {

  val columnNames: Seq[Char] = 'a' to 'z' take Board.FILES_IN_ROW

  def pieceToSymbol(p: Piece): Char = {
    val symbol = p.pieceType match {
      case King => 'K'
      case Pawn => 'P'
      case Rook => 'R'
      case Queen => 'Q'
      case Bishop => 'B'
      case Knight => 'N'
    }
    if (p.color == BlackPiece) symbol.toLower else symbol
  }

  def printColumnNames(): Unit = {
    print("  ")
    println(columnNames.mkString(""))
  }

  def printBoard(board: Board): Unit = {
    printColumnNames()
    println()
    board.rows().zipWithIndex.foreach {
      case (row, index) =>
        val rowName = rowIndexToName(index)
        print(rowName)
        print(" ")

        row.foreach { cell =>
          print(cell.map(pieceToSymbol).getOrElse(' '))
          print("")
        }
        print(" ")
        println(rowName)
    }
    println()
    printColumnNames()
  }

  def rowIndexToName(index: Int): String = (8 - index).toString
}
