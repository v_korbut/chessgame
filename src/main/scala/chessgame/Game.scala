package chessgame

import chessgame.model.BoardValidations._
import chessgame.model._

class Game(
  val board: Board = Board.initialBoard,
  val currentPlayer: PieceColor = WhitePiece,
  val inCheck: Boolean = false) {

  def applyMove(move: Move): Either[MoveValidationError, Game] = {
    val nextPlayer = currentPlayer.opponent

    for {
      _ <- board.validateMove(move, currentPlayer)
      newBoard = board.move(move.source, move.target)
      inCheck = newBoard.findPlayerInCheck
      _ <- inCheck.get(currentPlayer).map(Check).toLeft(())
    } yield new Game(newBoard, nextPlayer, inCheck.contains(nextPlayer))
  }
}
