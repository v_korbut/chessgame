package chessgame

import java.io.File

import chessgame.model.{Move, Position}

object InputFileReader {

  def readFile(file: File): LazyList[Move] = {
    val uif = new com.whitehatgaming.UserInputFile(file.getAbsolutePath)
    LazyList
      .continually(Option(uif.nextMove()))
      .takeWhile(_.isDefined)
      .flatten
      .map(parseMove)
  }

  def parseMove(position: Array[Int]): Move = {
    assert(position.length == 4) //TODO: return Option[(Position, Position)]
    val Array(cf, rf, ct, rt) = position
    Move(Position(cf, rf), Position(ct, rt))
  }
}
